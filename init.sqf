call compileFinal preprocessFileLineNumbers "FAR_revive\FAR_revive_init.sqf";
ltkCuratorSync = compile preprocessFileLineNumbers "LTK\ltkCuratorSync.sqf";

//placements_afb = compile preprocessFileLineNumbers "LTK\create\STATICS_AFB.sqf";
//placements = compile preprocessFileLineNumbers "LTK\create\placements.sqf";
placements = compile preprocessFileLineNumbers "LTK\create\STATICS_airfield_molos.sqf";

//DIVER_LYNX = compile preprocessFileLineNumbers "LTK\stuff\DIVER_LYNX.sqf";
//DIVER_MXSW = compile preprocessFileLineNumbers "LTK\stuff\DIVER_MXSW.sqf";
//DIVER_MXC = compile preprocessFileLineNumbers "LTK\stuff\DIVER_MXC.sqf";


/* Initialise the =BTC= Revive script */
//call compile preprocessFileLineNumbers "=BTC=_revive\=BTC=_revive_init.sqf";




//setViewDistance 3500;

/*if (!isDedicated) then
{
	waitUntil { alive player };
}*/


	/*respawn_west attachTo [sdv,[0,0,-10]];
	this addAction["<t color='#ff1111'>Virtual Ammobox</t>", "VAS\open.sqf"];
	submarine addAction["<t color='#ff1111'>Spawn Zodiac</t>", { _c = createVehicle ['B_Boat_Transport_01_F', [0,0,0], [], 0, 'NONE']; _c setPosATL [(getPosATL submarine) select 0,((getPosATL submarine) select 1 )+10,((getPosATL submarine) select 2)+10]; _c setDir (getDir submarine); }];*/


/*
	========== Copy All Unit Gear to Clipboard ==========

	Usage:
	Execute on any unit via init/trigger/other script, replacing [this] with [unitName] or [player] if needed:

	nul = [this] execVM "copyUnitGearToClipboard.sqf";
		nul = [player] execVM "copyUnitGearToClipboard.sqf";
		nul = [unit1] execVM "copyUnitGearToClipboard.sqf";

	When it executes a hint will be displayed showing the output and it is also copied to clipboard for you to paste somewhere else.

*/

/*
NUKE Related:

* copy the 3 triggers of the demomission on your mission
* place in you mission any object (if it should be a invisible object use invisible heli h) and name it nukepos
* create a new trigger in your mission with the condition you want where at the on activation field the variable "nuke_activated" will be set to true like: 
*/
	// NUKE RELATED STUFF
	mdh_nuke_destruction_zone	= 1000;	// DESTRUCTION ZONE OF NUKE IN METERS, USE 0 TO DEACTIVATE
	mdh_nuke_camshake		= 1;	// CAEMRASHAKE AT NUKEDETONATION 1=ON, 0=OFF
	mdh_nuke_ash			= 1;	// ASH AFTER NUKEDETONATION 1=ON, 0=OFF
	mdh_nuke_colorcorrection	= 1;	// COLLORCORRECTION AFTER NUKEDETONATION 1=ON, 0=OFF


if(isServer || isDedicated) then
{
	ltkCreatedObjects=[];
	[ltkCreatedObjects] spawn placements;
	[ltkCreatedObjects] spawn ltkCuratorSync;

	// [[]] spawn placements_afb;
};